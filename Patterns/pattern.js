//SQAURE STAR

let contentHere = document.getElementById("starContent");
console.log(contentHere);
for (let i = 0; i < 4; i++) {
  let line = "";
  for (let j = 0; j < 4; j++) {
    document.write = "* ";
    line += "* ";
  }
  console.log(line);
}

//PATTERN 2

//*
//* *
//* * *
//* * * *
//* * * * *

for (let i = 0; i < 5; i++) {
  let line = "";
  for (let j = 0; j <= i; j++) {
    line += "* ";
  }
  console.log(line);
}

//PATTERN 3
/*
1
12
123
1234
12345
*/

for (let i = 1; i <= 5; i++) {
  let line = "";
  for (let j = 1; j <= i; j++) {
    line += j;
  }
  console.log(line);
}

//PATTERN 4
/*
1
22
333
4444
55555
*/

// Online Javascript Editor for free
// Write, Edit and Run your Javascript code using JS Online Compiler

for (let i = 1; i <= 5; i++) {
  let line = "";
  for (let j = 1; j <= i; j++) {
    line += i;
  }
  console.log(line);
}

//PATTERN 5
/*
 * * * * * *
 * * * * *
 * * * *
 * * *
 * *
 *
 */

//Solution One
for (let i = 0; i <= 5; i++) {
  let line = "";
  for (let j = 0; j < 5 - i + 1; j++) {
    line += "* ";
  }
  console.log(line);
}

//Solution Two

for (let i = 5; i > 0; i--) {
  let line = "";
  for (let j = 1; j <= i; j++) {
    line += "* ";
  }
  console.log(line);
}

//PATTERN 5 NUMBER PATTERN INVERSE
/*
12345
1234
123
12
1
*/ for (let i = 0; i <= 5; i++) {
  let line = "";
  for (let j = 1; j < 5 - i + 1; j++) {
    line += j;
  }
  console.log(line);
}

//PATTERN 6 star triangle pattern
/*
 *
 ***
 *****
 *******
 *********
 */
for (let i = 0; i < 5; i++) {
  let line = "";
  for (let j = 0; j < 5 - i - 1; j++) {
    line += " ";
  }
  for (let j = 0; j < 2 * i + 1; j++) {
    line += "*";
  }
  for (let j = 0; j < 5 - i - 1; j++) {
    line += " ";
  }
  console.log(line);
}

//PATTERN 7 reverse star triangle
for (let i = 0; i < 5; i++) {
  let line = "";
  for (let j = 0; j < i; j++) {
    line += " ";
  }
  for (let j = 0; j < 2 * 5 - (2 * i + 1); j++) {
    line += "*";
  }
  for (let j = 0; j < i; j++) {
    line += " ";
  }
  console.log(line);
}

//PATTERN 8 Combinign abovetwo patterns

for (let i = 0; i < 5; i++) {
  let line = "";
  for (let j = 0; j < 5 - i - 1; j++) {
    line += " ";
  }
  for (let j = 0; j < 2 * i + 1; j++) {
    line += "*";
  }
  for (let j = 0; j < 5 - i - 1; j++) {
    line += " ";
  }
  console.log(line);
}

for (let i = 0; i < 5; i++) {
  let line = "";
  for (let j = 0; j < i; j++) {
    line += " ";
  }
  for (let j = 0; j < 2 * 5 - (2 * i + 1); j++) {
    line += "*";
  }
  for (let j = 0; j < i; j++) {
    line += " ";
  }
  console.log(line);
}

//PATTERN 10
/*
1
01
101
0101
10101
*/
for (let i = 0; i < 5; i++) {
  let line = "";
  let start = 0;
  if (i % 2 == 0) {
    start = 1;
  } else {
    start = 0;
  }
  for (let j = 0; j <= i; j++) {
    line += start;
    start = 1 - start;
  }
  console.log(line);
}
