/*
A valid parentheses string s is primitive if it is nonempty, and there does not exist a way to split it into s = A + B, with A and B nonempty valid parentheses strings.

Given a valid parentheses string s, consider its primitive decomposition: s = P1 + P2 + ... + Pk, where Pi are primitive valid parentheses strings.

Return s after removing the outermost parentheses of every primitive string in the primitive decomposition of s.

 

Example 1:

Input: s = "(()())(())"
Output: "()()()"
Explanation: 
The input string is "(()())(())", with primitive decomposition "(()())" + "(())".
After removing outer parentheses of each part, this is "()()" + "()" = "()()()".
*/

/*
Given two strings s and t, determine if they are isomorphic.

Two strings s and t are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.

 

Example 1:

Input: s = "egg", t = "add"
Output: true
*/

function isoMoprhicHandler(s, t) {
  let mapS = new Map();
  let mapT = new Map();

  for (let i = 0; i < s.length; i++) {
    if (mapS.has(s[i])) {
      if (mapS.get(s[i]) != t[i]) {
        return false;
      }
    } else {
      mapS.set(s[i], t[i]);
    }
    if (mapT.has(t[i])) {
      if (mapT.get(t[i]) != s[i]) {
        return false;
      }
    } else {
      mapT.set(t[i], s[i]);
    }
  }
  return true;
}

console.log(isoMoprhicHandler("foo", "bar"));
/*
Given two strings s and goal, return true if and only if s can become goal after some number of shifts on s.

A shift on s consists of moving the leftmost character of s to the rightmost position.

For example, if s = "abcde", then it will be "bcdea" after one shift.
 

Example 1:

Input: s = "abcde", goal = "cdeab"
Output: true
Example 2:

Input: s = "abcde", goal = "abced"
Output: false
*/

function canBecomeGoalAfterShifts(s, goal) {
  if (s.length !== goal.length) {
      return false;
  }
  return (s + s).includes(goal);
}

// Test cases
console.log(canBecomeGoalAfterShifts("abcde", "cdeab")); // Output: true
console.log(canBecomeGoalAfterShifts("abcde", "abced")); // Output: false