/*
Count the occurrence of keys and convert the result into array of objects where each object belongs to one key and it's occurrence (count).
[
    { language: 'JavaScript' }, { language: 'JavaScript' }, { language: 'TypeScript' }, { language: 'C++' }
] 
*/
let data = [
  { language: "JavaScript" },
  { language: "JavaScript" },
  { language: "TypeScript" },
  { language: "C++" },
];

let dataObj = {};

for (let i = 0; i < data.length; i++) {
  if (dataObj[data[i].language]) {
    dataObj[data[i].language]++;
  } else {
    dataObj[data[i].language] = 1;
  }
}
console.log(dataObj);
let result = Object.keys(dataObj).map((key) => {
  return { language: key, count: dataObj[key] };
});
console.log(result);

/*
Find the missing number in a given integer array of 1 to 10
*/
// Given array
// let numberArr = [1, 2, 3, 4, 5, 10];

// Function to find the first missing number
// function findFirstMissingNumber(arr) {
//     for (let i = 1; i <= 10; i++) {
//         let found = false;
//         for (let j = 0; j < arr.length; j++) {
//             if (arr[j] === i) {
//                 found = true;
//                 break;
//             }
//         }
//         if (!found) {
//             return i;
//         }
//     }
//     return null; // Return null if no number is missing (all numbers 1 to 10 are present)
// }

// let firstMissing = findFirstMissingNumber(numberArr);
// console.log(firstMissing); // Output the first missing number

// Given array
let numberArr = [1, 2, 3, 4, 5, 10];

// Full range array from 1 to 10
let fullRange = Array.from({ length: 10 }, (_, i) => i + 1);

// Find missing numbers
let missingNumbers = fullRange.filter((num) => !numberArr.includes(num));

console.log(missingNumbers);

/*
 Find a duplicate number in an array of integers
 const arr = [1,2,3,4,5,6,7,7,8,6,10];
*/
const duplicatearr = [1, 2, 3, 4, 5, 6, 7, 7, 8, 6, 10];
let uniqueObj = {};
let count = 0;
for (let i = 0; i < duplicatearr.length; i++) {
  if (uniqueObj[duplicatearr[i]]) {
    console.log(uniqueObj[duplicatearr[i]]);
  } else {
    uniqueObj[duplicatearr[i]] = duplicatearr[i];
  }
}
console.log(uniqueObj);

/*
Return an array showing the cumulative sum at each index of an array of integers
*/
let needToFindcummSumArr = [1, 3, 5, 7];
let cummulativeSum = 0;
for (let i = 0; i < needToFindcummSumArr.length; i++) {
  cummulativeSum += needToFindcummSumArr[i];
}

console.log(cummulativeSum);

/*
Find all duplicate numbers in an array with multiple duplicates
*/

const duparr = [1, 1, 2, 3, 4, 5, 6, 7, 8, 6, 6, 7, 7, 7, 10, 10];
let dupObj = {};
let filterDupArr = [];
for (let i = 0; i < duparr.length; i++) {
  if (dupObj[duparr[i]]) {
    dupObj[duparr[i]]++;
  } else {
    dupObj[duparr[i]] = 1;
  }
}
console.log(dupObj);

Object.keys(dupObj).map((item) => {
  console.log(item, dupObj[item]);
  if (dupObj[item] > 1) {
    filterDupArr.push(item);
  }
});
console.log(filterDupArr);

/*
Find all pairs in an array of integers whose sum is equal to a given number
let arr = [1,5,6,1,0,1];
*/
let sumArr = [1, 5, 6, 1, 0, 1];
let pairsArr = [];
const pairsSet = new Set();
function sumArrHandler(sumArr, target) {
  for (let i = 0; i < sumArr.length; i++) {
    let currentVal = sumArr[i];

    for (let j = i + 1; j < sumArr.length; j++) {
      let sum = currentVal + sumArr[j];
      if (target == sum) {
        pairsArr.push([currentVal, sumArr[j]]);
      }
    }
  }
}

sumArrHandler(sumArr, 6);
console.log(pairsArr);
console.log(pairsSet);

/*
const add(5)(4)(3)()
*/

let curryingHandler = function numOneHandler(numOne) {
  return function (numtwo) {
    if (numtwo) {
      return numOneHandler(numOne + numtwo);
    }
    return numOne;
  };
};
console.log(curryingHandler(5)(4)(3)());

/*
let addsix = createBase(6)
addsix(10) // 16
*/

function createBase(base) {
  return function newBase(num) {
    return num + base;
  };
}
let addSix = createBase(6);
console.log(addSix(10));

var Employee = {
  company: "xyz",
};
var emp1 = Object.create(Employee);
console.log(emp1.company); //it can also be access with__proto__ because in this prototype  has been added and delete does note delete  from protottype

let newArr = [1, 23, 4, 5, 7, 85];
console.log(newArr.slice(0, 4)); // Slice return new array does not modify the original array, but splice does modify original array
console.log(newArr);

/*
 Check for Palindrome
  */
let pallStr = "racecar";
function pallindromeHandler(pallStr) {
  let reversedStr = "";
  for (let i = pallStr.length - 1; i >= 0; i--) {
    reversedStr += pallStr[i];
  }
  return reversedStr === pallStr;
}
console.log(pallindromeHandler(pallStr));

let pushArr = [1, 2, 3, 5, 8, 45, 8, 9];
console.log(pushArr.pop());

let nums = [1, 2, 3, 1];
console.log([...new Set(nums)]);

/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
 */

let twoSum = function (nums, target) {
  let sumArr = [];
  for (let i = 0; i < nums.length; i++) {
    let currentValue = nums[i];
    for (j = i + 1; j < nums.length; j++) {
      let sum = currentValue + nums[j];
      if (sum == target) {
        sumArr.push(i, j);
        break;
      }
    }
  }
  return sumArr;
};

let anotherVar = 10;
{
  let anotherVar = 20;
  console.log(anotherVar);
}
console.log(anotherVar);

//nfe (named function expression)
var Foo = function Bar() {
  return 7;
};
console.log(Foo, typeof Foo, Foo());
// typeof Bar(); // Reference error because function already has one reference which is Foo

//memoization
function memoizeHandler() {
  let obj = {};
  return (value) => {
    if (value in obj) {
      console.log("cached");
      return obj[value];
    } else {
      console.log("calculate");
      let result = value + 20;
      obj[value] = result;
      return result;
    }
  };
}

let memoize = memoizeHandler();

console.log(memoize(10)); // calculate
console.log(memoize(10)); // cached
console.log(memoize(40)); //calculate

const products = [
  { id: 2, name: "Laptop", price: 500 },
  { id: 2, name: "Phone", price: 300 },
  { id: 3, name: "Headphones", price: 100 },
];

// indexOf (might not work as expected)
const searchProduct1 = { id: 1, name: "Laptop", price: 500 }; // New object instance
const indexOfLaptop = products.indexOf(searchProduct1); // Returns -1 (not the same object)

// findIndex with a condition on the id property
const findProductId2 = products.findIndex((product) => product.id === 2);
console.log(findProductId2); // Output: 1 (index of the object with id 2)

// function x(){
//   for (var i = 1; i <= 10; i++){
//     function close(i){
//       setTimeout(function (){
//       console.log(i);
//       }, i * 1000);
//     }
//     close(i);
//   }
//   console.log("Learn")
// }
// x();

// const akash = 'paliwal'
// akash = 'paliwal2'
// console.log(akash)

/*
Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

 

Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
*/

let strs = ["eat", "tea", "tan", "ate", "nat", "bat"];

function groupAnagrams(strs) {
  let obj = {};

  for (let i = 0; i < strs.length; i++) {
    let sortedStr = strs[i].split("").sort().join("");
    if (obj[sortedStr]) {
      obj[sortedStr].push(strs[i]);
    } else {
      obj[sortedStr] = [strs[i]];
    }
  }
  return Object.values(obj);
}

console.log(groupAnagrams(strs));

/*
Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

 

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
*/

let numsArr = [1, 1, 1, 2, 2, 2, 2, 3];
function mostFrequentElements(numsArr, k) {
  let map = new Map();
  console.log(map);
  for (let num of numsArr) {
    map.set(num, (map.get(num) || 0) + 1);
  }
  console.log(map);
  let list = [...map];
  list.sort((a, b) => b[1] - a[1]);
  let ans = [];
  for (let i = 0; i < k; i++) {
    ans.push(list[i][0]);
  }
  return ans;
}

console.log(mostFrequentElements(numsArr, 2));

/*
Given an integer array nums of length n, you want to create an array ans of length 2n where ans[i] == nums[i] and ans[i + n] == nums[i] for 0 <= i < n (0-indexed).

Specifically, ans is the concatenation of two nums arrays.

Return the array ans.

 

Example 1:

Input: nums = [1,2,1]
Output: [1,2,1,1,2,1]
Explanation: The array ans is formed as follows:
- ans = [nums[0],nums[1],nums[2],nums[0],nums[1],nums[2]]
- ans = [1,2,1,1,2,1]
*/
let needToConcatArr = [1, 2, 1];

function concatArray(needToConcatArr) {
  let result = new Array(needToConcatArr.length * 2);

  for (let i = 0; i < needToConcatArr.length; i++) {
    result[i] = needToConcatArr[i];
    result[i + needToConcatArr.length] = needToConcatArr[i];
    console.log(result);
  }
  return result;
}

console.log(concatArray(needToConcatArr));

/*
Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.

 

Example 1:

Input: nums = [1,2,3,4,5,6,7], k = 3
Output: [5,6,7,1,2,3,4]
*/

let rotateArr = [1, 2, 3, 4, 5, 6, 7];

function rotateArrHandler(rotateArr, k) {
  // let temp = rotateArr[0]                        //GOOD FOR ONE ROTATION
  // for(let i=k;i<rotateArr.length;i++) {
  //   rotateArr[i-k] = rotateArr[i]
  // }
  // rotateArr[rotateArr.length - 1] = temp

  //multiple rotation
  console.log(rotateArr);
  let temp = rotateArr.slice(0, k);
  for (let i = k; i < rotateArr.length; i++) {
    rotateArr[i - k] = rotateArr[i];
  }
  let j = 0;
  for (let i = rotateArr.length - k; i < rotateArr.length; i++) {
    rotateArr[i] = temp[j];
    j++;
  }
}

// rotateArrHandler(rotateArr, 1)
rotateArrHandler(rotateArr, 3);
console.log(rotateArr);

/*
Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Note that you must do this in-place without making a copy of the array.

 

Example 1:

Input: nums = [0,1,0,3,12]
Output: [1,3,12,0,0]
*/

let moveZeroArr = [0, 1, 0, 3, 12];

function moveZeroHandler(moveZeroArr) {
  let zeroArr = moveZeroArr.filter((item) => item == 0);
  let nonZerArr = moveZeroArr.filter((item) => item != 0);
  let result = [...nonZerArr, ...zeroArr];
  return result;
}
console.log(moveZeroHandler(moveZeroArr));

/*
Given two sorted arrays of size n and m respectively, find their union. The Union of two arrays can be defined as the common and distinct elements in the two arrays. Return the elements in sorted order.
*/

let union1 = [1, 2, 3, 8, 4];
let union2 = [1, 2, 3, 6, 7];
function sortedUnionHandler(union1, union2) {
  //INBUILT FUNCTIONS
  let result = [...union1, ...union2];
  return [...new Set(result)].sort((a, b) => a - b);
}

console.log(sortedUnionHandler(union1, union2));

/*
Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.

 

Example 1:

Input: nums = [3,0,1]
Output: 2
Explanation: n = 3 since there are 3 numbers, so all numbers are in the range [0,3]. 2 is the missing number in the range since it does not appear in nums.
*/
let missingNumbersArr = [9,6,4,2,3,5,7,0,1];
function missingNumbersArrHanlder(missingNumbersArr) {
  let number = 0;
  for (let i = 0; i < missingNumbersArr.length; i++) {
    if (!missingNumbersArr.includes(i)) {
      number = i;
    }
  }
  return number;
}
console.log(missingNumbersArrHanlder(missingNumbersArr));

/*
Given a binary array nums, return the maximum number of consecutive 1's in the array.

 

Example 1:

Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The maximum number of consecutive 1s is 3.
*/
let consecutiveOnesArr = [1,1,0,1,1,1]
function consecutiveOnesArrHandler(consecutiveOnesArr) {
  let counter = 0
  for(let i=0;i<consecutiveOnesArr.length;i++) {
    if(consecutiveOnesArr[i] == 1) {
      counter++
    } else {
      counter = 0
    }
  }
  return counter
}
console.log(consecutiveOnesArrHandler(consecutiveOnesArr))

/*
Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.

 

Example 1:

Input: nums = [2,2,1]
Output: 1
Example 2:

Input: nums = [4,1,2,1,2]
Output: 4
*/

let exceptOne = [4,1,2,1,2]
let exceptOneObj = {}

function exceptOneHandler(exceptOne) {
  for(let i=0;i<exceptOne.length;i++) {
    if(exceptOneObj[exceptOne[i]]) {
      exceptOneObj[exceptOne[i]]++
    } else {
      exceptOneObj[exceptOne[i]] = 1
    }
  }
}
exceptOneHandler(exceptOne)

Object.keys(exceptOneObj).map((item) => {
  if(exceptOneObj[item] == 1) {
    console.log(item)
  }
})

/*
Given an array arr containing n integers and an integer k. Your task is to find the length of the longest Sub-Array with the sum of the elements equal to the given value k.

 
for (let i = 0; i < sumArr.length; i++) {
    let currentVal = sumArr[i];

    for (let j = i + 1; j < sumArr.length; j++) {
      let sum = currentVal + sumArr[j];
      if (target == sum) {
        pairsArr.push([currentVal, sumArr[j]]);
      }
    }
  }


Examples:
 

Input :
arr[] = {10, 5, 2, 7, 1, 9}, k = 15
Output : 4
Explanation:
The sub-array is {5, 2, 7, 1}.
*/

let longestSubArr = [10, 5, 2, 7, 1, 9]
let resultArr = []
function longestSubArrHandler(arr, target) {
  let result = []; // Array to store subarrays with sum equal to target
  let longest = 0

  for (let i = 0; i < arr.length; i++) {
    let currentSum = arr[i]; // Keep track of current subarray sum
    let subarray = [arr[i]]; // Subarray starting from current element

    for (let j = i + 1; j < arr.length; j++) {
      currentSum += arr[j];
      subarray.push(arr[j]);

      if (currentSum === target) {
        // Found a subarray with sum equal to target
        result.push(subarray.slice()); // Push a copy of the subarray to avoid mutation
        longest = Math.max(longest, subarray.length);
      }
    }
  }
 return longest
}
const target = 15;
resultArr = longestSubArrHandler(longestSubArr, target);
console.log(resultArr)


/*
Several people are standing in a row and need to be divided into two teams. The first person goes into team 1, the second goes into team 2, the third goes into team 1 again, the fourth into team 2, and so on.

You are given an array of positive integers - the weights of the people. Return an array of two integers, where the first element is the total weight of team 1, and the second element is the total weight of team 2 after the division is complete.

Example

For a = [50, 60, 60, 45, 70], the output should be
solution(a) = [180, 105].
*/
let weightArr = [50, 60, 60, 45, 70]
function weightArrHandler(weightArr) {
  let temp1 = 0
  let temp2 = 0
  for(let i=0;i<weightArr.length;i++) {
    if(i%2 == 0) {
      temp1 += weightArr[i]
    } else{
      temp2 += weightArr[i]
    }
  }
  let result = [temp1, temp2]
  return result
}

console.log(weightArrHandler(weightArr))