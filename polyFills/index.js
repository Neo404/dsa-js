//poluyfill for MAp
function customMap(cb) {
    let result = []
    let self = this
    for(let i=0;i<self.length;i++) {
        result.push(cb(self[i], i, self))
    }
    return result
}

Array.prototype.customMap = customMap

let arrMap = [1,2,3].customMap((item) => {
    return item * 2;
})

console.log(arrMap)

//poluyfill for filter
function customfilter(cb) {
    let result = []
    let self = this
    for(let i=0;i<self.length;i++) {
        if(cb(self[i],i ,self))
            result.push(self[i])
    }
    return result
}

Array.prototype.customFilter = customfilter
let filterArr = [1,2,3].customFilter((item) => {
    return item > 1;
})
console.log(filterArr)